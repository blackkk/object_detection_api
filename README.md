This project includes a basic python SDK that provides necessary methods to load a trained Tensorflow Object Detection model and run an inference for a given video.
In addition, I wrote an interface from which you can implement and set it as a listener to invoke onDetect() function whenever the model is done processing a frame
of the video.