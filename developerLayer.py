import os
import sys
import cv2

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")

import myclasses


def onKaskDetection(detections):
    th = 0.9

    for i in range(detections['meta']['kask']):
        if( detections['objects']['kask'][i][0] >= th ):
            return True

    return False

def callback(frame):
    cv2.imshow("KASKLI_FRAME",frame)
    if cv2.waitKey(1) == ord('q'):
        cv2.destroyAllWindows()

def onNoGozluk(detections):
    th = 0.95

    for i in range(detections['meta']['gozluk']):
        if( detections['objects']['gozluk'][i][0] < th ):
            return True

    return False

def callback2(frame):
    cv2.imshow("GOZLUKSUZ",frame)
    if cv2.waitKey(1) == ord('q'):
        cv2.destroyAllWindows()



def main():
    
    CWD_PATH = os.getcwd()
    PATH_TO_CKPT = os.path.join(CWD_PATH,'inference_graph','frozen_inference_graph.pb')
    PATH_TO_LABELS = os.path.join(CWD_PATH,'training','labelmap.pbtxt')

    NUM_CLASSES = 9


    VIDEO_NAME = 'test.mp4'
    PATH_TO_VIDEO = os.path.join(CWD_PATH,VIDEO_NAME)

    detectObj = myclasses.Detection(PATH_TO_CKPT,PATH_TO_LABELS,NUM_CLASSES)

    detectObj.setVideo(PATH_TO_VIDEO)

    detectObj.addConditionalCapture( onKaskDetection, callback )
    detectObj.addConditionalCapture( onNoGozluk , callback2 )
    detectObj.start()

    print("Main thread doing something else")

    return


if __name__ == '__main__':
    main()