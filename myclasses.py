import os
import sys
import cv2
import numpy as np
import tensorflow as tf

from scipy import ndimage
from threading import Thread


# Import utilites
from utils import label_map_util
from utils import visualization_utils as vis_util

class Detection:


	def __init__(self,model_path,labelmap_path,num_classes):
		self.model_path = model_path
		self.labelmap_path = labelmap_path
		self.num_classes = num_classes

		self.captureTuples = list()

		self.video = None

		label_map = label_map_util.load_labelmap(labelmap_path)
		categories = label_map_util.convert_label_map_to_categories(label_map, 
			max_num_classes=self.num_classes, use_display_name=True)
		self.category_index = label_map_util.create_category_index(categories)

	
		self.detection_graph = tf.Graph()

		with self.detection_graph.as_default():
		    od_graph_def = tf.GraphDef()
		    with tf.gfile.GFile(model_path, 'rb') as fid:
		        serialized_graph = fid.read()
		        od_graph_def.ParseFromString(serialized_graph)
		        tf.import_graph_def(od_graph_def, name='')

	def addConditionalCapture(self,condition,callback):
		self.captureTuples.append( ( condition, callback))


	def setVideo(self,path):
		self.video = cv2.VideoCapture(path)

	def start(self):

		if self.video is None:
			raise ValueError("No video set!")

		t1 = Thread(target = self.processVideo , args=())
		t1.start()




	def processVideo(self):

		with tf.Session(graph = self.detection_graph) as sess:

			image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')

			output_tensors = []

			output_tensors.append(self.detection_graph.get_tensor_by_name('detection_boxes:0'))
			output_tensors.append(self.detection_graph.get_tensor_by_name('detection_scores:0'))
			output_tensors.append(self.detection_graph.get_tensor_by_name('detection_classes:0'))
			output_tensors.append(self.detection_graph.get_tensor_by_name('num_detections:0'))


			frame_width = int(self.video.get(3))
			frame_height = int(self.video.get(4))

			#new_width = int(frame_width/2)
			#new_height = int(frame_height/2)

			while(self.video.isOpened()):

				ret, frame = self.video.read()   
				if ret == True:

					frame = ndimage.rotate(frame, 270)
					#frame = cv2.resize(frame, (new_height, new_width))

					
					(boxes, scores, classes, num) = self.processFrame(frame,sess,image_tensor,output_tensors)
					#boxes: koordinatlar, score: yuzdelikler, classes: hangi obje oldugu int, num: index

					detections = {'meta' : { },
								  'objects': { }}

					# 1 -> yelek
					# 2 -> kask
					# 3 -> gozluk
					# 4 -> eldiven
					# 5 -> isci
					# 6 -> yelek_yok
					# 7 -> kask_yok
					# 8 -> isci_yok
					# 9 -> eldiven_yok

					l1 = []
					l2 = []
					l3 = []
					l4 = []
					l5 = []
					l6 = []
					l7 = []
					l8 = []
					l9 = []
					for i in range(int(num[0])):
						if (classes[0][i] == 1):
							l1.append([scores[0][i], list(boxes[0][i])])
						elif (classes[0][i] == 2):
							l2.append([scores[0][i], list(boxes[0][i])])
						elif (classes[0][i] == 3):
							l3.append([scores[0][i], list(boxes[0][i])])
						elif (classes[0][i] == 4):
							l4.append([scores[0][i], list(boxes[0][i])])
						elif (classes[0][i] == 5):
							l5.append([scores[0][i], list(boxes[0][i])])
						elif (classes[0][i] == 6):
							l6.append([scores[0][i], list(boxes[0][i])])
						elif (classes[0][i] == 7):
							l7.append([scores[0][i], list(boxes[0][i])])
						elif (classes[0][i] == 8):
							l8.append([scores[0][i], list(boxes[0][i])])
						elif (classes[0][i] == 9):
							l9.append([scores[0][i], list(boxes[0][i])])


					detections['objects'].update({self.category_index[1]['name']: l1,
												  self.category_index[2]['name']: l2,
												  self.category_index[3]['name']: l3,
												  self.category_index[4]['name']: l4,
												  self.category_index[5]['name']: l5,
												  self.category_index[6]['name']: l6,
												  self.category_index[7]['name']: l7,
												  self.category_index[8]['name']: l8,
												  self.category_index[9]['name']: l9})
					del (l1)
					del (l2)
					del (l3)
					del (l4)
					del (l5)
					del (l6)
					del (l7)
					del (l8)
					del (l9)

					detections['meta'].update({self.category_index[1]['name']: len(detections['objects'][self.category_index[1]['name']]),
											   self.category_index[2]['name']: len(detections['objects'][self.category_index[2]['name']]),
											   self.category_index[3]['name']: len(detections['objects'][self.category_index[3]['name']]),
											   self.category_index[4]['name']: len(detections['objects'][self.category_index[4]['name']]),
											   self.category_index[5]['name']: len(detections['objects'][self.category_index[5]['name']]),
											   self.category_index[6]['name']: len(detections['objects'][self.category_index[6]['name']]),
											   self.category_index[7]['name']: len(detections['objects'][self.category_index[7]['name']]),
											   self.category_index[8]['name']: len(detections['objects'][self.category_index[8]['name']]),
											   self.category_index[9]['name']: len(detections['objects'][self.category_index[9]['name']])})

					for captureTuple in self.captureTuples:
						if ( captureTuple[0](detections) ):
							captureTuple[1](frame)

			



	def processFrame(self,frame,sess,image_tensor,output_tensors):
		
		image_expanded = np.expand_dims(frame, axis=0)

		return sess.run(
		    output_tensors,feed_dict={image_tensor: image_expanded})


